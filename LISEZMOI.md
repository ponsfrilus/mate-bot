# Mate Bot

Code d'un bot [Telegram](https://t.me) permettant de gérer la consommation de 
[Club Mate](https://fr.wikipedia.org/wiki/Club-Mate) entre différentes 
personnes.


# Mise en place

Afin de pouvoir tester le bot, suivez les étapes suivantes:
  1. Copier `secret.sample.json` vers `secret.sample`
  1. Communiquer avec [@BotFather](https://t.me/BotFather) afin de créer un bot
  et d'obtenir le token
  1. Remplacez la clé `telegramToken` dans le fichier `secret.sample` avec le
  token du bot
  1. Installer [telegraf](https://telegraf.js.org) et
  [update-json-file](https://www.npmjs.com/package/update-json-file) avec la
  commande:  
    `npm i telegraf update-json-file`
  1. Lancer le script `node index.js`
  1. Parler à votre bot sur Telegram, par exemple `/people`