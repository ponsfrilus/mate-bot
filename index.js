const Telegraf = require('telegraf')
const Extra = require('telegraf/extra')
const Markup = require('telegraf/markup')
var secret = require('./secret.json');
var data = require('./data.json');
const filePath = './data.json'
const bot = new Telegraf(secret.telegramToken)
const updateJsonFile = require('update-json-file')


bot.use(Telegraf.log())

bot.command('help', ({
    reply
  }) =>
  reply('• /people → Get info for specific person\n• /price → Get some prices \n• /mate → Post when you take a Mate')
)

bot.command('cmd', ({
    reply
  }) =>
  reply('What do you want to do ?', Markup
    .keyboard([['/people', '/info']])
    .oneTime()
    .extra()
  )
)

// bot.command('mate', ({
//   reply
// }) => {
//   return reply('Custom buttons keyboard', Markup
//     .keyboard([
//       ['💰 prix', '🍾 info'], // Row1 with 2 buttons
//     ])
//     .extra()
//   )
// })

bot.command('price', ({
  reply
}) => {
  return reply('• 1 Mate → CHF 2.0 \n• 1 Box (20 Mate) → CHF 40.0')
})

// bot.hears('🍾 info', ctx => ctx.reply('Quel caisse ?'))


bot.command('people', (ctx) => {
  return ctx.replyWithHTML('Who ?', Extra.markup(
    Markup.keyboard([
      [
        'Nicolas R.',
        'Nicolas B.',
        'Loïc H.'
      ],
      [
        'All'
      ]
    ])
  ))
})

bot.hears('Nicolas R.', ctx => ctx.reply('Nicolas R →   ' + (data.NicolasR + 2)))
bot.hears('Nicolas B.', ctx => ctx.reply('Nicolas B →   ' + data.NicolasB))
bot.hears('Loïc H.', ctx => ctx.reply('Loïc H →         ' + data.LoicH))
bot.hears('All', ctx => ctx.reply('• Nicolas R →   ' + data.NicolasR + '\n• Nicolas B →   ' + data.NicolasB + '\n• Loïc H    →   ' + data.LoicH))


bot.command('mate', (ctx) => {
  return ctx.reply('What is your request ?', Extra.HTML().markup((m) =>
    m.inlineKeyboard([
      m.callbackButton('I\'ve take a Mate (+1)', '-1'),
      m.callbackButton('I\'ve done a misteke (-1)', '-1')
    ])))
})


bot.hears(/\/wrap (\d+)/, (ctx) => {
  return ctx.reply('Keyboard wrap', Extra.markup(
    Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
      columns: parseInt(ctx.match[1])
    })
  ))
})



bot.action(/.+/, (ctx) => {
  return ctx.answerCbQuery(`${ctx.match[0]}`)
})

bot.launch()
